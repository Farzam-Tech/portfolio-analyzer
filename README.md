# Portfolio Analyzer 

## Background

An Investment planning tool that will use the Yahoo Finance API & Alpaca API to fetch historical closing prices for a  portfolio composed of stocks and cryptocurrencies, then run Monte Carlo simulations to project the portfolio performance at 30 years. You will then use the Monte Carlo data to calculate the expected portfolio returns given a specific initial investment amount.

an analysis notebook that analyzes and visualizes the major metrics of the portfolios to determine which portfolio is performing the best across multiple areas: volatility, returns, risk, and Sharpe ratios, and also determine which portfolio outperformed the others.


### Technologies
* Python 
* JupiterLab
* Pandas to read and clean the data
* numpy to do the calculations
* Panel , Panle.interact, plotly , hvplot ,matplotlib,pyplot for visualization


### Resources

* The **Yahoo Finance API** will be used to pull historical stocks and cryptocurrencies information.

* The **Alpaca Markets API** will be used to pull historical stocks and cryptocurrencies information.  

* [AlpacaDOCS](https://alpaca.markets/docs/)

* [YahooFinance](https://ca.finance.yahoo.com/)

* [Pandas API Docs](https://pandas.pydata.org/pandas-docs/stable/reference/index.html)

* [Exponential weighted function in Pandas](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.ewm.html)



### Conduct Quantitative Analysis

Analyze the data to see if any of the portfolios outperform the stock market

#### Performance Analysis

1. Calculate and plot daily returns of all portfolios.

2. Calculate and plot cumulative returns for all portfolios. Does any portfolio outperform the Markets

#### Risk Analysis

1. Create a box plot for each of the returns. 

2. Calculate the standard deviation for each portfolio. 

3. Determine which portfolios are riskier than the S&P TSX 60

4. Calculate the Annualized Standard Deviation.

#### Rolling Statistics

1. Calculate and plot the rolling standard deviation for all portfolios using a 21-day window.

2. Calculate and plot the correlation between each stock 

3. Choose one portfolio, then calculate and plot the 60-day rolling beta for it

#### Rolling Statistics Challenge: Exponentially Weighted Average

An alternative method to calculate a rolling window is to take the exponentially weighted moving average. This is like a moving window average, but it assigns greater importance to more recent observations. 

### Sharpe Ratios

Investment managers and their institutional investors look at the return-to-risk ratio, not just the returns. After all, if you have two portfolios that each offer a 10% return, yet one is lower risk, you would invest in the lower-risk portfolio

1. Using the daily returns, calculate and visualize the Sharpe ratios using a bar plot.

2. Determine whether the algorithmic strategies outperform the market




